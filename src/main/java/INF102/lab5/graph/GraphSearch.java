package INF102.lab5.graph;

import java.util.*;

/**
 * This class is used to conduct search algorithms of a graph
 */
public class GraphSearch<V> implements IGraphSearch<V> {

    private IGraph<V> graph;

    public GraphSearch(IGraph<V> graph) {
        this.graph = graph;
    }
    // graph search algorithms. DFS vs BFS to determine whether two nodes are connected
    @Override
    public boolean connected(V u, V v) { // can v be reached from u
        Queue<V> queue = new LinkedList();
        Set<V> visited = new HashSet<>();
        queue.add(u);
        while (!queue.isEmpty()){ // remove front node from the queue if it's not empty
            V currentNode = queue.poll();
            if (currentNode.equals(v)){ // if the front node removed equals node v there is a path to v from that node
                return true;
            }
            else {
                visited.add(currentNode); // if not we just mark it as visited
            }
            for (V neighbors : graph.getNeighbourhood(currentNode)) {
                if (!visited.contains(neighbors)){
                    queue.add(neighbors);
                }
            }
        }
        return false;
    }

}
