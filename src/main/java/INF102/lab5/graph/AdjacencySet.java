package INF102.lab5.graph;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class AdjacencySet<V> implements IGraph<V> {

    private Set<V> nodes;
    private Map<V, Set<V>> nodeToNode; // edges and neighbors

    public AdjacencySet() {
        nodes = new HashSet<>();
        nodeToNode = new HashMap<>();
    }

    @Override
    public int size() {
        return nodes.size();
    }

    @Override
    public Set<V> getNodes() {
        return Collections.unmodifiableSet(nodes);
    }

    @Override
    public void addNode(V node) {
        if (!nodeToNode.containsKey(node)){
            nodes.add(node);
            nodeToNode.put(node, new HashSet<>()); // add the node to the map but with empty set
        }
    }

    @Override
    public void removeNode(V node) {
        if(hasNode(node)){
            for (V neighbor : nodeToNode.get(node)){
                nodeToNode.get(neighbor).remove(node);
            }
        }
        nodeToNode.remove(node);
        nodes.remove(node);
    }

    @Override
    public void addEdge(V u, V v) {
        if (!hasNode(u) || !hasNode(v)){
            throw new IllegalArgumentException("You can't add an edge to a node that doesn't exist on the graph.");
        }
        nodeToNode.get(u).add(v); // add u to the set of v
        nodeToNode.get(v).add(u);
    }

    @Override
    public void removeEdge(V u, V v) {
        nodeToNode.get(u).remove(v);
        nodeToNode.get(v).remove(u);
    }

    @Override
    public boolean hasNode(V node) {
        return nodes.contains(node);
    }

    @Override
    public boolean hasEdge(V u, V v) {
        Set<V> neighbours = nodeToNode.get(u);
        return neighbours != null && nodeToNode.get(u).contains(v);
    }

    @Override
    public Set<V> getNeighbourhood(V node) {
        return Collections.unmodifiableSet(nodeToNode.get(node));
    }

    @Override
    public String toString() {
        StringBuilder build = new StringBuilder();
        for (V node : nodeToNode.keySet()) {
            Set<V> nodeList = nodeToNode.get(node);

            build.append(node);
            build.append(" --> ");
            build.append(nodeList);
            build.append("\n");
        }
        return build.toString();
    }

}
